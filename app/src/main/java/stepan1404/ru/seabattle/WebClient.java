package stepan1404.ru.seabattle;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface WebClient {
    @POST("/api/results")
    @Headers("Content-Type: application/json")
    Call<UserDto> newStat(@Body UserDto name);

    @GET("/api/results/{name}")
    Call<UserDto> getStats(@Path("name") String name);

    @GET("/api/results")
    Call<UserDto> getAllStats();
}
