package stepan1404.ru.seabattle;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.text.InputFilter;
import android.text.InputType;
import android.view.SurfaceView;
import android.widget.EditText;
import android.widget.Toast;

import com.example.SeaBattle.R;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ivan on 18.11.2014.
 */

/**
 * View board and ships(model).
 */
public class BattleView extends SurfaceView {
    //background
    private Bitmap bitmap;
    private Paint paint;
    private WebClient client;
    /**
     * Use true - player is making move, false - pc
     * used to determine who making move and red appropriate
     * (red/green) circle notification.
     */
    private boolean turn = true;
    /**
     * 1 if player won
     * 2 if enemy won,
     * -1 if nobody have won yet.
     */
    private int winner = -1;
    //dynamic cell size for viewing at miscellaneous screens.
    private static int cellSize;
    //start of  player's board
    private int xStart = 0, yStart = 0;
    //
    private int endEnemyFieldY;
    private int startEnemyFieldX;
    //updated model
    private int[][] player;
    private int[][] enemy;

    public BattleView(Context context) {
        super(context);
        paint = new Paint();
        setWillNotDraw(false);
        bitmap = BitmapFactory.decodeResource(
                getResources(),
                R.drawable.battle1);
    }

    public void setClient(WebClient client) {
        this.client = client;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawRect(0, 0, getWidth(), getHeight(), paint);
        canvas.drawBitmap(bitmap, null, new RectF(0, 0, canvas.getWidth(), canvas.getHeight()), null);
        canvas.save();
        drawBoard(canvas);
        canvas.restore();
        canvas.save();
        drawShips(canvas);
        canvas.restore();
        checkWinner(canvas);
    }

    private boolean checkWinner(Canvas canvas) {
        if (winner != -1) {
            paint.setColor(Color.BLACK);
            paint.setTextSize(96f);
            //canvas.drawRect(150, 150, getWidth() * 0.7f, getHeight() * 0.7f, paint);
            if (winner == 1) {
                String text = "You won!";
                float textWidth = paint.measureText(text);
                canvas.drawText(text, getWidth() / 2 - textWidth / 2, getHeight() / 2, paint);

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Enter your name");

                final EditText input = new EditText(getContext());
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                input.setFilters(new InputFilter[] {
                        (src, start, end, dst, dstart, dend) -> {
                            if(src.equals("")){ // for backspace
                                return src;
                            }
                            if(src.toString().matches("^[A-Za-z0-9-_ ]*$")){
                                return src;
                            }
                            return "";
                        }
                });
                builder.setView(input);

                builder.setPositiveButton("OK", (dialog, which) -> {
                    Call<UserDto> call = client.newStat(new UserDto(input.getText().toString()));
                    call.enqueue(new Callback<UserDto>() {
                        @Override
                        public void onResponse(Call<UserDto> call, Response<UserDto> response) {
                            if (response.code() != 200) {
                                Toast.makeText(getContext(), getContext().getString(R.string.backend_internal_error), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getContext(), getContext().getString(R.string.statistic_write_success), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<UserDto> call, Throwable t) {
                            Toast.makeText(getContext(), getContext().getString(R.string.try_later), Toast.LENGTH_SHORT).show();
                        }
                    });

                });
                builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());

                builder.show();
            } else {
                String text = "Game over!";
                float textWidth = paint.measureText(text);
                canvas.drawText(text, getWidth() / 2 - textWidth / 2, getHeight() / 2, paint);
            }
            return true;
        }
        return false;
    }

    private void drawShips(Canvas canvas) {
        Paint paint = new Paint();
        for (int i = 0; i < BattleField.N; ++i) {
            for (int j = 0; j < BattleField.N; ++j) {
                String text = String.format(Locale.US, "%c%d", i + 65, j);
                paint.setARGB(150, 0, 0, 0);
                paint.setTextSize(36f);
                float textWidth = paint.measureText(text) / 2;
                canvas.drawText(text, j * cellSize + cellSize / 2 - textWidth, i * cellSize + cellSize / 2 + 12, paint);

                if (player[i][j] == BattleField.SHIP) {
                    paint.setARGB(150, 0, 150, 100);
                    canvas.drawRect(j * cellSize, i * cellSize,
                            (j + 1) * cellSize, (i + 1) * cellSize, paint);
                } else if (player[i][j] == BattleField.ATTACKED) {
                    paint.setARGB(150, 0, 0, 100);
                    canvas.drawRect(j * cellSize, i * cellSize,
                            (j + 1) * cellSize, (i + 1) * cellSize, paint);
                } else if (player[i][j] == BattleField.ATTACKED_SHIP) {
                    paint.setARGB(150, 250, 0, 0);
                    canvas.drawRect(j * cellSize, i * cellSize,
                            (j + 1) * cellSize, (i + 1) * cellSize, paint);
                }
            }
        }
        canvas.translate(startEnemyFieldX, 0);

        for (int i = 0; i < BattleField.N; ++i) {
            for (int j = 0; j < BattleField.N; ++j) {
                String text = String.format(Locale.US, "%c%d", i + 65, j);
                paint.setARGB(150, 0, 0, 0);
                paint.setTextSize(36f);
                float textWidth = paint.measureText(text) / 2;
                canvas.drawText(text, j * cellSize + cellSize / 2 - textWidth, i * cellSize + cellSize / 2 + 12, paint);

                if (enemy[i][j] == BattleField.ATTACKED) {
                    paint.setARGB(150, 0, 0, 100);
                    //rp.setARGB(150, 196, 0, 171);
                    canvas.drawRect(j * cellSize, i * cellSize,
                            (j + 1) * cellSize, (i + 1) * cellSize, paint);
                } else if (enemy[i][j] == BattleField.ATTACKED_SHIP) {
                    paint.setARGB(150, 250, 0, 0);
                    canvas.drawRect(j * cellSize, i * cellSize,
                            (j + 1) * cellSize, (i + 1) * cellSize, paint);
                }
            }
        }
    }

    private void drawBoard(Canvas canvas) {
        cellSize = getWidth() / (2 * BattleField.N) - 8;
        paint.setAntiAlias(true);
        paint.setColor(Color.WHITE);
        //draw player
        int n = BattleField.N * cellSize;
        //vertical lines
        for (int i = 0; i <= n; i += cellSize) {
            canvas.drawLine(i, yStart, i, n, paint);
        }
        //horizontal lines
        for (int i = 0; i <= n; i += cellSize) {
            canvas.drawLine(xStart, i, n, i, paint);
        }
        //draw indicator
        if (turn) {
            paint.setColor(Color.GREEN);
        } else {
            paint.setColor(Color.RED);
        }

        canvas.drawCircle(getWidth() / 2, getHeight() / 2, 10, paint);
        //draw enemy
        startEnemyFieldX = getWidth() - n;
        endEnemyFieldY = cellSize * BattleField.N;
        canvas.translate(startEnemyFieldX, 0);
        paint.setColor(Color.BLACK);
        //vertical lines
        for (int i = 0; i <= n; i += cellSize) {
            canvas.drawLine(i, yStart, i, n, paint);
        }
        //horizontal lines
        for (int i = 0; i <= n; i += cellSize) {
            canvas.drawLine(xStart, i, n, i, paint);
        }
    }


    public void setBoard(int[][] player, int[][] enemy) {
        this.player = new int[BattleField.N][BattleField.N];
        this.enemy = new int[BattleField.N][BattleField.N];
        for (int i = 0; i < BattleField.N; ++i)
            for (int j = 0; j < BattleField.N; ++j) {
                this.player[i][j] = player[i][j];
                this.enemy[i][j] = enemy[i][j];
            }
    }

    public void setTurn(boolean t) {
        this.turn = t;
    }

    public void setWinner(int winner) {
        this.winner = winner;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // Defines the extra padding for the shape name text
        int contentWidth = 100;
        // Resolve the width based on our minimum and the measure spec
        int minw = contentWidth + getPaddingLeft() + getPaddingRight();
        int w = resolveSizeAndState(minw, widthMeasureSpec, 0);
        // Ask for a height that would let the view get as big as it can
        int shapeHeight = 100;
        int minh = shapeHeight + getPaddingBottom() + getPaddingTop();
        int h = resolveSizeAndState(minh, heightMeasureSpec, 0);
        // Calling this method determines the measured width and height
        // Retrieve with getMeasuredWidth or getMeasuredHeight methods later
        setMeasuredDimension(w, h);
    }

    public int getEndEnemyFieldY() {
        return endEnemyFieldY;
    }

    public int getCellSize() {
        return cellSize;
    }

    public int getStartEnemyFieldX() {
        return startEnemyFieldX;
    }
}
