package stepan1404.ru.seabattle;

public class UserDto {
    private String name;

    public Long getWinCount() {
        return winCount;
    }

    public void setWinCount(Long winCount) {
        this.winCount = winCount;
    }

    public UserDto(String name, Long winCount) {
        this.name = name;
        this.winCount = winCount;
    }

    private Long winCount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserDto(String name) {
        this.name = name;
    }

    public UserDto() {
    }
}
