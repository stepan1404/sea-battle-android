package stepan1404.ru.seabattle;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.text.InputFilter;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.SeaBattle.R;

import java.util.ArrayList;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import static android.os.SystemClock.sleep;

/**
 * Main activity class, can be interpreted as a controller to view and model.
 * Provides updating model and view, handling touches.
 */
public class SeaBattleMain extends Activity implements View.OnTouchListener {
    private final int REQ_CODE_SPEECH_INPUT = 100;

    private BattleView mBattleView;
    private SeaBattleGame mSeaBattleGame;

    volatile boolean playerTurn = true;
    volatile boolean enemyTurn = false;
    private WebClient client;


    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initApp();

    }

    private void initApp() {
        setContentView(R.layout.main);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.web_host))
                .addConverterFactory(JacksonConverterFactory.create())
                .build();

        client = retrofit.create(WebClient.class);
    }

    /**
     * Handler to click "Start" button at the main layout;
     *
     * @param v Current view.
     */
    public void goToBattle(View v) {
        mBattleView = new BattleView(this);
        mBattleView.setOnTouchListener(this);
        mBattleView.setClient(client);
        setContentView(mBattleView);
        mSeaBattleGame = new SeaBattleGame();
        //update view model
        mBattleView.setBoard(mSeaBattleGame.getPlayerPlacement(),
                mSeaBattleGame.getEnemyPlacement());
        mBattleView.invalidate();
    }

    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, getString(R.string.speech_prompt));

        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(), getString(R.string.speech_not_supported), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent e) {
        int x = (int) e.getX();
        int y = (int) e.getY();
        int cellSize = mBattleView.getCellSize();
        int startBoardX = mBattleView.getStartEnemyFieldX();
        int endBoardX = cellSize * BattleField.N + startBoardX;
        int endBoardY = mBattleView.getEndEnemyFieldY();
        switch (e.getAction()) {
            case MotionEvent.ACTION_DOWN:
                //click only over enemy board to hit
                if (x >= startBoardX && y <= endBoardY &&
                        x <= endBoardX) {
                    int ip = y / cellSize;
                    int jp = (x - startBoardX) / cellSize;
                    return playerAttackEnemy(ip, jp);
                }

            case MotionEvent.ACTION_UP:
                enemyAttackPlayer();
            case MotionEvent.ACTION_MOVE:
                //user incorrect touch
        }
        return true;
    }

    /**
     * Attack enemy's ships.
     *
     * @param i row position of cell
     * @param j column position of cell
     * @return True, that means end of player's attack
     * and then enemy will hit, cause ACTION.UP called after
     * ACTION.DOW return true;
     * False, if attack was successul and player will attack
     * again, cause of game rules.
     */
    private boolean playerAttackEnemy(int i, int j) {
        mBattleView.setTurn(playerTurn);
        mBattleView.invalidate();
        if (playerTurn) {
            if (!mSeaBattleGame.attackEnemy(i, j)) {
                //unsuccessful
                mBattleView.setTurn(false);
                mBattleView.setBoard(mSeaBattleGame.getPlayerPlacement(),
                        mSeaBattleGame.getEnemyPlacement());
                //in fact draw red circle that means enemy turn now
                mBattleView.invalidate();
                //turn enemy move
                enemyTurn = true;
                playerTurn = false;
                return true;
            }
            //check winner
            int winner = mSeaBattleGame.getWinner();
            if (winner != -1) {
                mBattleView.setWinner(winner);
                mBattleView.setBoard(mSeaBattleGame.getPlayerPlacement(),
                        mSeaBattleGame.getEnemyPlacement());
                mBattleView.invalidate();
                return false;
            }
            mBattleView.setTurn(true);
            mBattleView.setBoard(mSeaBattleGame.getPlayerPlacement(),
                    mSeaBattleGame.getEnemyPlacement());
            mBattleView.invalidate();
            return false;
        }
        return false;
    }

    private boolean enemyAttackPlayer() {
        if (enemyTurn) {
            mBattleView.setTurn(false);
            mBattleView.invalidate();
            //while enemy is attacking only player's ships
            while (mSeaBattleGame.attackPlayer()) {
                mBattleView.setBoard(mSeaBattleGame.getPlayerPlacement(),
                        mSeaBattleGame.getEnemyPlacement());
                //wait and give info to player that "now enemy is thinking"
                try {
                    sleep(500);
                } catch (Exception ie) {
                }
                int winner = mSeaBattleGame.getWinner();
                if (winner != -1) {
                    mBattleView.setWinner(winner);
                    mBattleView.setBoard(mSeaBattleGame.getPlayerPlacement(),
                            mSeaBattleGame.getEnemyPlacement());
                    mBattleView.invalidate();
                    return false;
                }
            }
            //hitting finished, cause enemy fluffed
            mBattleView.setBoard(mSeaBattleGame.getPlayerPlacement(),
                    mSeaBattleGame.getEnemyPlacement());
            playerTurn = true;
            mBattleView.setTurn(true);
            mBattleView.invalidate();
            enemyTurn = false;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    /**
     * Receiving speech input
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> resultList = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    String section = null;

                    for (String s : resultList) {
                        if (s.matches("^[a-jA-J][0-9]$")) {
                            section = s;
                            break;
                        }
                    }

                    if (section != null) {
                        int i = section.toUpperCase().charAt(0) - 65;
                        int j = section.charAt(1) - 48;
                        playerAttackEnemy(i, j);
                        enemyAttackPlayer();
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.try_again), Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            }

        }
    }

    /**
     * Can restart the game;
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings: {
                initApp();
                break;
            }
            case R.id.voice_input: {
                promptSpeechInput();
                break;
            }
            case R.id.get_stats: {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Enter your name");

                final EditText input = new EditText(this);
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                input.setFilters(new InputFilter[] {
                        (src, start, end, dst, dstart, dend) -> {
                            if(src.equals("")){ // for backspace
                                return src;
                            }
                            if(src.toString().matches("^[A-Za-z0-9-_ ]*$")){
                                return src;
                            }
                            return "";
                        }
                });
                builder.setView(input);


                builder.setPositiveButton("OK", (dialog, which) -> {
                    Call<UserDto> call = client.getStats(input.getText().toString());
                    call.enqueue(new Callback<UserDto>() {
                        @Override
                        public void onResponse(Call<UserDto> call, Response<UserDto> response) {
                            if (response.code() != 404) {
                                if (response.body() != null) {
                                    UserDto dto = response.body();
                                    Toast.makeText(getApplicationContext(), String.format(Locale.US, "Player: %s.\nWins: %d", dto.getName(), dto.getWinCount()), Toast.LENGTH_SHORT).show();
                                    System.out.println();
                                } else {
                                    Toast.makeText(getApplicationContext(), getString(R.string.backend_internal_error), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), getString(R.string.player_not_found), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<UserDto> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), getString(R.string.try_later), Toast.LENGTH_SHORT).show();
                        }
                    });

                });
                builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());

                builder.show();
            }
        }
        return true;
    }
}
